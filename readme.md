# Upload PrimeKG Datasets to Data Platform

0. create directory "data-platform",then download datasets from <https://drive.google.com/drive/folders/1Us6IxSseHG5yFXUpwcIQGCCMdnRSjVAM?usp=share_link/> to directory "data-platform"
1. get token from <https://discovery.data.storemesh.com/>
2. replace your ID of **landing directory** and **integration derectory**
3. Initial DataNode
4. upload raw files to "landing directory"
5. read file with **Dask.DataFrame**
6. upload data files to 'integretion directory' with **Dask.DataFrame** file
